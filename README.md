# Projet-Python : IA playing Snake

Pour ce projet, j'ai décidé de faire une IA qui apprend à jouer au jeu 'Snake'.

## Le jeu du serpent 

Dans un premier temps, j'ai du recréer le jeu grace à Pygame.
Le jeu est composé d'une classe SnakeGame et grâce à plusieurs fonctions, on peut jouer grâce aux flèches du clavier au Snake.

Le jeux est jouable grâce au fichier snakegame.py 
et pour lancer l'apprentissage il fait faire la commande python agent.py

## l'intelligence artificielle

Je suis donc partie du jeu que je venais de créer et j'ai du créer 3 nouveaux fichiers : agent.py, helper.py et model.py. 

helper.py : il sert simplement à définir une fonction plot() qui permet d'afficher les statistiques de mon IA.

model.py et agent.py : ces fichiers utilisent Torch pour créer/entrainer le modèle de l'IA. J'ai utilisé le Deep Q learning qui est de l'apprentissage renforcé (apprentissage avec un système de récompense) qui utilise un réseau de neuronnes.
Le réseau de neuronnes est composé de :
- 11 inputs [
    
    - s'il y a un danger (mur ou sois mêm) [devant, à droite ou à gauche]
    - la direction dans laquelle la tête du serpent va [vers la droite, vers la gauche, vers le haut, vers le bas]
    - où est la nouriture par rapport à la tête du serpent [vers la droite, vers la gauche, vers le haut, vers le bas]

]
- une couche cachée
- 3 outputs ( les 3 posibilités du joueur [tout droit, tourner à gauche, tourner à droite])










